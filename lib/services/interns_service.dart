import 'dart:convert';
import 'package:adn_flutter/models/Intern.dart';
import 'package:http/http.dart' as http;

fetchInterns(year, month) async {
  try {
    var y = year;
    var x = month;

    var url = "http://192.168.1.9:3333/interns/year/$y/$x";
    var _uri = Uri.parse(url);

    var response = await http.get(_uri);
    var body = response.body;
    var json = jsonDecode(body);
    print(body);

    return InternData.fromJson(jsonDecode(body));
  } catch (e) {
    print(e);
  }
}
