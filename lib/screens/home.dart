import 'package:adn_flutter/providers/intern.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:adn_flutter/providers/user.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var month;
  var year;

  @override
  Widget build(BuildContext context) {
    var state = context.watch<LogoutProvider>();
    var intern = context.watch<InternProvider>();
    return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              icon: Icon(Icons.logout, color: Colors.deepOrangeAccent,),
              tooltip: 'Open shopping cart',
              onPressed: () async {
                context.read<LogoutProvider>().logout();

                if (state.revoked != null) {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/', (Route<dynamic> route) => false);
                }
              },
            )
          ],
          backgroundColor: Colors.white,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Card(
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                          child: DropdownButtonFormField<String>(
                            decoration: const InputDecoration(
                              labelText: 'Month',
                            ),
                            value: month,
                            items: [
                              'Jan',
                              'Feb',
                              'Mar',
                              'Apr',
                              'May',
                              'Jun',
                              'Jul',
                              'Aug',
                              'Sept',
                              'Oct',
                              'Nov',
                              'Dec'
                            ]
                                .map((label) => DropdownMenuItem(
                                      child: Text(label),
                                      value: label,
                                    ))
                                .toList(),
                            onChanged: (value) {
                              setState(() {
                                month = value;
                              });
                            },
                          ),
                        ),
                      ),
                      Flexible(
                        child: DropdownButtonFormField<String>(
                          decoration: const InputDecoration(
                            labelText: 'Year',
                          ),
                          value: year,
                          items: [
                            '2000',
                            '2001',
                            '2002',
                            '2003',
                            '2004',
                            '2006',
                            '2007',
                            '2008',
                            '2009',
                            '2010',
                            '2011',
                            '2012',
                            '2013',
                            '2014',
                            '2015',
                            '2016',
                            '2017',
                            '2018',
                            '2019',
                            '2020',
                            '2021'
                          ]
                              .map((label) => DropdownMenuItem(
                                    child: Text(label),
                                    value: label,
                                  ))
                              .toList(),
                          onChanged: (value) {
                            setState(() {
                              year = value;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Center(
                  child: FlatButton(
                      child: Text('Get Interns'),
                      onPressed: () {
                        context.read<InternProvider>().getInterns(year, month);
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                          side: BorderSide(color: Colors.deepOrange)))),
              Flexible(
                child: Interns(intern: intern),
              )
            ],
          ),
        ));
  }
}

class Interns extends StatelessWidget {
  const Interns({
    Key key,
    @required this.intern,
  }) : super(key: key);

  final InternProvider intern;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount:
              (MediaQuery.of(context).orientation == Orientation.portrait)
                  ? 2
                  : 3),
      itemCount: intern.interns.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              Navigator.pushNamed(context, '/details');
            },
            child: Card(
              elevation: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.width / 4,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                            "https://user-images.githubusercontent.com/57434209/117569486-971ddb80-b0ce-11eb-9ace-d5c475240d2c.png"),
                        fit: BoxFit.fitWidth,
                        alignment: Alignment.topCenter,
                      ),
                    ),
                  ),
                  ListTile(
                    title: Text("${intern.interns[index].name}",  style: TextStyle(fontSize: 10),),
                    subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Text(
                            "${intern.interns[index].email}",
                            style: TextStyle(fontSize: 8, ),
                          ),
                        ]),
                  
                  ),
                ],
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
            ),
          ),
        );
      },
    );
  }
}
