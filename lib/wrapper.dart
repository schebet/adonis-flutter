import 'package:adn_flutter/auth/auth_wrapper.dart';
import 'package:adn_flutter/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:adn_flutter/providers/user.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var state = context.watch<UserProvider>();
    var stateSignIn = context.watch<SignUpProvider>();
    var user = state.user;
    var userS = stateSignIn.user;
    
    if (user == null) {
      return AuthWrapper();
    } else {
      return Home();
    }
  }
}
