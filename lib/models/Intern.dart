class InternData {
  List<Interns> interns;

  InternData({this.interns});

  InternData.fromJson(Map<String, dynamic> json) {
    if (json['interns'] != null) {
      interns = new List<Interns>();
      json['interns'].forEach((v) {
        interns.add(new Interns.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.interns != null) {
      data['interns'] = this.interns.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Interns {
  int id;
  String name;
  int year;
  String email;
  String createdAt;
  String updatedAt;
  String month;

  Interns(
      {this.id,
      this.name,
      this.year,
      this.email,
      this.createdAt,
      this.updatedAt,
      this.month});

  Interns.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    year = json['year'];
    email = json['email'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    month = json['month'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['year'] = this.year;
    data['email'] = this.email;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['month'] = this.month;
    return data;
  }
}
