# adn_flutter

Flutter application that consumes Adonis API

## Screenshots
<p float="left">
<img src="https://user-images.githubusercontent.com/57434209/117570002-101e3280-b0d1-11eb-8dbe-19f5b7e9b061.jpg" width="250" />
<img src="https://user-images.githubusercontent.com/57434209/117570022-2a581080-b0d1-11eb-87b2-4619faf8c66b.jpg" width="250" />
<img src="https://user-images.githubusercontent.com/57434209/117570043-4a87cf80-b0d1-11eb-8a4c-38756e6473f4.jpg" width="250" />
</p>



## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
