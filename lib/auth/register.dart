import 'package:adn_flutter/providers/user.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SignUp extends StatefulWidget {
  final Function toggleview;

  SignUp({this.toggleview});
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    var state = context.watch<SignUpProvider>();

    return Scaffold(
      body: Container(
        child: Center(
          child: Theme(
            data: ThemeData(
              primaryColor: Colors.deepOrange,
            ),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                                text: "I",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 45,
                                    fontFamily: "Cormorant Upright",
                                    color: Colors.redAccent)),
                            TextSpan(
                                text: "nterns",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 35,
                                    fontFamily: "Cormorant Upright",
                                    color: Colors.black.withOpacity(0.6))),
                            TextSpan(
                                text: "I",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 45,
                                    fontFamily: "Cormorant Upright",
                                    color: Colors.redAccent)),
                            TextSpan(
                                text: "NFO",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 35,
                                    fontFamily: "Cormorant Upright",
                                    color: Colors.black.withOpacity(0.6))),
                          ],
                        ),
                      ),
                      Form(
                        key: _formKey,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              TextFormField(
                                  decoration: const InputDecoration(
                                    icon: Icon(Icons.mail),
                                    hintText: '',
                                    labelText: 'Email',
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    return null;
                                  },
                                  onChanged: (val) {
                                    setState(() {
                                      email = val.trim();
                                    });
                                  }),
                              TextFormField(
                                  decoration: const InputDecoration(
                                    icon: Icon(Icons.vpn_key),
                                    hintText: '',
                                    labelText: 'Password',
                                  ),
                                  validator: (value) => value.length < 6
                                      ? 'Enter a password 6+ long'
                                      : null,
                                  obscureText: true,
                                  onChanged: (val) {
                                    setState(() {
                                      password = val;
                                    });
                                  }),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 16.0),
                                child: RaisedButton(
                                  onPressed: () async {
                                    // Validate returns true if the form is valid, or false
                                    // otherwise.

                                    if (_formKey.currentState.validate()) {
                                      context
                                          .read<SignUpProvider>()
                                          .signUp(email, password);
                                      if (state.user != null) {
                                        // Navigator.pushNamed(context, '/home');
                                        Navigator.pushNamed(context,
                                          '/home',
                                        );
                                      }
                                    }
                                  },
                                  child: Text('Sign Up'),
                                  color: Colors.deepOrange,
                                  textColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                      side: BorderSide(color: Colors.red)),
                                ),
                              ),
                              //SizedBox(height: 60,),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton(
                    onPressed: () {
                      widget.toggleview();
                    },
                    child: Text(
                      "Already have an account?",
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                        side: BorderSide(color: Colors.deepOrange)),
                  ),
                ),
              ],
            ),
          ),
        ),
        /* add child content here */
      ),
    );
  }
}
