import 'dart:convert';
import 'package:adn_flutter/models/Status.dart';
import 'package:adn_flutter/models/User.dart';
import 'package:http/http.dart' as http;

Future<User> userLogin(String email, String password) async {
  try {
    var cred = {"email": email, "password": password};
    var _url = Uri.parse('http://192.168.1.9:3333/login/');

    var response = await http.post(_url,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: cred,
        encoding: Encoding.getByName("utf-8"));

    var body = response.body;
    var token = User.fromJson(jsonDecode(body));

    print(token.token);

    return User.fromJson(jsonDecode(body));
  } catch (e) {
    
  print(e);
  }
}

Future<Revoked> userLogout() async {
  try {
    var _url = Uri.parse('http://192.168.1.9:3333/logout');

    var response = await http.post(_url,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        encoding: Encoding.getByName("utf-8"));

    var body = response.body;
    return Revoked.fromJson(jsonDecode(body));

   
  } catch (e) {
    throw 'Error';
  }
}

Future<User> userRegister(email, password) async {
  try {
    var cred = {"email": email, "password": password};
    print(cred);
    var _url = Uri.parse('http://192.168.1.9:3333/register');

    var response = await http.post(_url,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: cred,
        encoding: Encoding.getByName("utf-8"));

   print(response.body);

    if (response.statusCode == 200) {
      var result = userLogin(email, password);
      print(result);
  
       return result;
   }
  } catch (e) {
    return e;
  }
}
