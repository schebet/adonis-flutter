import 'package:adn_flutter/providers/intern.dart';
import 'package:adn_flutter/providers/user.dart';
import 'package:flutter/material.dart';
import 'package:adn_flutter/services/route.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
            providers: [
        ChangeNotifierProvider(
          create: (ctx) => UserProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => SignUpProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => LogoutProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => InternProvider(),
        ),

      ],
          child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: "/",
        onGenerateRoute: generateRoute,
      ),
    );
  }
}
