class Revoked {
  bool revoked;

  Revoked({this.revoked});

  Revoked.fromJson(Map<String, dynamic> json) {
    revoked = json['revoked'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['revoked'] = this.revoked;
    return data;
  }
}