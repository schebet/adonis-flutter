import 'package:adn_flutter/auth/auth_wrapper.dart';
import 'package:adn_flutter/auth/login.dart';
import 'package:adn_flutter/auth/register.dart';
import 'package:adn_flutter/screens/home.dart';
import 'package:flutter/material.dart';


Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/':
      return MaterialPageRoute(builder: (context) => AuthWrapper());
    case '/home':
      return MaterialPageRoute(builder: (context) => Home());
    case '/login':
      return MaterialPageRoute(builder: (context) => SignIn());

  }
}
