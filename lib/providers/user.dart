import 'package:adn_flutter/models/Status.dart';
import 'package:adn_flutter/models/User.dart';
import 'package:flutter/material.dart';
import 'package:adn_flutter/services/http_service.dart';

class UserProvider with ChangeNotifier {
  User _user;
  User get user => _user;
  Future<void> login(String email, String password) async {
    try {
      _user = (await userLogin(email, password));
      print(_user);
      notifyListeners();
    } catch (e) {
      //handle error
      print(e);
      notifyListeners();
    }
  }
}

class SignUpProvider with ChangeNotifier {
  User _user;
  User get user => _user;

  Future<void> signUp(String email, String password) async {
    try {
      _user = (await userRegister(email, password));
      notifyListeners();
    } catch (e) {
      //handle error
      print(e);
      notifyListeners();
    }
  }
}
class LogoutProvider with ChangeNotifier {
  Revoked _revoked;
  Revoked get revoked => _revoked;

  Future<void> logout() async {
    try {
      _revoked = (await userLogout());
      notifyListeners();
    } catch (e) {
      //handle error
      print(e);
      notifyListeners();
    }
  }
}
