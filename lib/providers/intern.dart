import 'package:adn_flutter/models/Intern.dart';
import 'package:adn_flutter/services/interns_service.dart';
import 'package:flutter/material.dart';


class InternProvider with ChangeNotifier {
    List<Interns> _interns = [];


  List<Interns> get interns => _interns;
  Future<void> getInterns(year, month) async {
    try {
      _interns = (await fetchInterns(year, month)).interns;
      print(_interns);
      notifyListeners();
    } catch (e) {
      //handle error
      print(e);
      notifyListeners();
    }
  }
}