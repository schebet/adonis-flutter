import 'package:flutter/material.dart';
import 'package:adn_flutter/auth/login.dart';
import 'package:adn_flutter/auth/register.dart';


class AuthWrapper extends StatefulWidget {
  @override
  _AuthWrapperState createState() => _AuthWrapperState();
}

class _AuthWrapperState extends State<AuthWrapper> {
  bool showSignIn = false;

  void toggleView(){
    setState(() {
      
      showSignIn=!showSignIn;
    });
  }
  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return SignIn(toggleview: toggleView);
    } else {
      return SignUp(toggleview :toggleView);
    }
  }
}